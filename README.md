
Examples

Status:
ohm/controller/klorollo/status {"vbat_begin":3.783355,"vbat_late":3.80463,"bootcount":36,"esp_timer_time":16208,"rssi":-71,"temperature":28.33333,"hall":24,"version":"0.6","buildtime":"2019-03-17-16:10:34","reset_reason_0":5,"reset_reason_1":14,"wakeup_reason":5}

After wakeup go into wifi AP mode and stay in power on!
mosquitto_pub -h homeassistant -r -t 'ohm/controller/klorollo/trigger' -m "forceconfig"

Set new config:
mosquitto_pub -h homeassistant -r -f config.json -t 'ohm/controller/klorollo/config'
# Wakeup
mosquitto_pub -h homeassistant -r -n -t 'ohm/controller/klorollo/config'

