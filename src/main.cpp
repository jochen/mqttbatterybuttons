#include <Arduino.h>
#include <EEPROM.h>
#include "Update.h"
#include <ArduinoJson.h>
#include <WiFi.h>
#include <string>

extern "C"
{
#include "freertos/FreeRTOS.h"
#include "freertos/timers.h"
}


//#include "ESPAsyncTCP.h"
#include <ESPAsyncWebServer.h>
#include <ESPAsyncWiFiManager.h>
#include <AsyncMqttClient.h>
#include <FastLED.h>

#include "soc/soc.h"`
#include "soc/rtc_cntl_reg.h"`
#include <rom/rtc.h>



#ifdef __cplusplus
  extern "C" {
#endif

  uint8_t temprature_sens_read();

#ifdef __cplusplus
}
#endif

uint8_t temprature_sens_read();

/*

#include <stdio.h>
#include "rom/ets_sys.h"
//#include "soc/rtc_cntl_reg.h"
#include "soc/sens_reg.h"

float temperatureReadFixed(){
  SET_PERI_REG_BITS(SENS_SAR_MEAS_WAIT2_REG, SENS_FORCE_XPD_SAR, 3, SENS_FORCE_XPD_SAR_S);
  SET_PERI_REG_BITS(SENS_SAR_TSENS_CTRL_REG, SENS_TSENS_CLK_DIV, 10, SENS_TSENS_CLK_DIV_S);
  CLEAR_PERI_REG_MASK(SENS_SAR_TSENS_CTRL_REG, SENS_TSENS_POWER_UP);
  CLEAR_PERI_REG_MASK(SENS_SAR_TSENS_CTRL_REG, SENS_TSENS_DUMP_OUT);
  SET_PERI_REG_MASK(SENS_SAR_TSENS_CTRL_REG, SENS_TSENS_POWER_UP_FORCE);
  SET_PERI_REG_MASK(SENS_SAR_TSENS_CTRL_REG, SENS_TSENS_POWER_UP);
  ets_delay_us(100);
  SET_PERI_REG_MASK(SENS_SAR_TSENS_CTRL_REG, SENS_TSENS_DUMP_OUT);
  ets_delay_us(5);
  float temp_f = (float)GET_PERI_REG_BITS2(SENS_SAR_SLAVE_ADDR3_REG, SENS_TSENS_OUT, SENS_TSENS_OUT_S);
  float temp_c = (temp_f - 32) / 1.8;
  return temp_c;
}
*/

#define ENABLE_PRINT

#ifndef ENABLE_PRINT
// disable Serial output
#define Serial SerialDummy
static class
{
public:
  void begin(...) {}
  void print(...) {}
  void println(...) {}
  void setDebugOutput(...) {}
  void flush(...) {}
  void printf(...) {}
} Serial;

#endif

#define DEBUGOUT // Comment to enable debug output

#define DBG_OUTPUT Serial

#ifdef DEBUGOUT
#define DEBUGLOG(...) DBG_OUTPUT.printf(__VA_ARGS__)
#else
#define DEBUGLOG(...)
#endif

#define XSTR(x) STR(x)
#define STR(x) #x

#ifndef BUILDTIME
#define BUILDTIME "none"
#endif
#pragma message("BUILDTIME=" XSTR(BUILDTIME))

#define MAXTOPICLENGTH 128
#define MAXPAYLOADLENGTH 30

esp_sleep_wakeup_cause_t wakeup_reason;
int adcpin = 34; //gpio34 adc1_6
float startvbat = 0;
bool doing_update = false;
bool force_config = false;
const char *version = "0.6";
long rssi;
float temperature;
int hall;
int do_subscripe_firmwaretopic_id = 0;
int last_mqtt_package_id = 0;
int last_publish_success_id = 0;
const char *buildtime = (const char *)BUILDTIME;
#define TIMEOUT_SECONDS_DEFAULT 15
uint8_t timeout_seconds = TIMEOUT_SECONDS_DEFAULT;
unsigned long timeout_millis = 0;

void *jsonMessage;

#define DEFAULT_TOUCH_THRESHOLD 40 /* Greater the value, more the sensitivity */
touch_pad_t wakeupTouchPin;
uint8_t wakeupTouchGPIO;

AsyncMqttClient mqttClient;
//TimerHandle_t mqttReconnectTimer;
//TimerHandle_t wifiReconnectTimer;

AsyncWebServer server(80);
DNSServer dns;

extern unsigned long crc(uint8_t *blob, unsigned int size);

struct hostConfig
{
  IPAddress _ip;
  IPAddress _gw;
  IPAddress _sn;
  IPAddress mqttHost;
  uint16_t mqttPort;
  //char mqttUser[40];
  //char mqttPass[40];
  char configTopic[50];

  unsigned long crc;
} myhostConfig;

struct buttonPinConfig
{
  uint8_t touch;
  gpio_num_t logic;
  gpio_num_t ledPower;
};

#define BUTTONCOUNT 2
//Touch Pad 5 (GPIO12)
//Touch Pad 6 (GPIO14)
buttonPinConfig buttonsConfig[] = {
    {T5, GPIO_NUM_22, GPIO_NUM_19},
    {T6, GPIO_NUM_23, GPIO_NUM_18}
    };

#define COLORSORT GRB
#define LED_PIN_DATA GPIO_NUM_21
#define DEFAULT_BRIGHTNESS 32
CRGB leds[BUTTONCOUNT];

enum class BUTTON_STATE : uint8_t
{
  OFF = 0x00,
  SHORT_PRESSED = 0x01,
  LONG_PRESSED = 0x02,
};

struct buttonTargetConfig
{
  char Topic[MAXTOPICLENGTH] = "";
  char Value[MAXPAYLOADLENGTH] = "";
  char LongpressValue[MAXPAYLOADLENGTH] = "";
};

#define LONGPRESSMILLIS 3000
struct appConfig
{
  buttonTargetConfig buttons[BUTTONCOUNT];
  char firmwaretopic[MAXTOPICLENGTH] = "";
  uint8_t led_brightness = DEFAULT_BRIGHTNESS;
  uint16_t touchThreshold = DEFAULT_TOUCH_THRESHOLD;
  uint16_t touchWakeupThreshold = DEFAULT_TOUCH_THRESHOLD;
  unsigned long time_to_sleep;
  unsigned long crc = 0;
} myappConfig;

bool buttonPressed[BUTTONCOUNT] = {false};
//bool button2Pressed = false;
unsigned long buttonLongpressed[BUTTONCOUNT] = {0};
unsigned long buttonPause[BUTTONCOUNT] = {0};
//unsigned long button2Longpressed = 0;




//flag for saving data
bool shouldSaveConfig = false;
//callback notifying us of the need to save config
void saveConfigCallback()
{
  Serial.println(F("Save config\n"));
  shouldSaveConfig = true;
}

#define uS_TO_S_FACTOR 1000000    /* Conversion factor for micro seconds to seconds */
#define DEFAULT_TIME_TO_SLEEP 900 /* Time ESP32 will go to sleep (in seconds) */

RTC_DATA_ATTR int bootCount = 0;
RTC_DATA_ATTR int32_t esp_timer_time = 0;


/*
Method to print the reason by which ESP32
has been awaken from sleep
*/
void print_wakeup_reason()
{
  //esp_sleep_wakeup_cause_t wakeup_reason;

  wakeup_reason = esp_sleep_get_wakeup_cause();

  switch (wakeup_reason)
  {
  case ESP_SLEEP_WAKEUP_EXT0: //2
    Serial.println("Wakeup caused by external signal using RTC_IO");
    break;
  case ESP_SLEEP_WAKEUP_EXT1: //3
    Serial.println("Wakeup caused by external signal using RTC_CNTL");
    break;
  case ESP_SLEEP_WAKEUP_TIMER: //4
    Serial.println("Wakeup caused by timer");
    for (uint8_t button = 0; button < BUTTONCOUNT; button++)
    {
      leds[button] = CRGB::DeepPink;
    }
    FastLED.setBrightness(myappConfig.led_brightness);
    FastLED.show();
    break;
  case ESP_SLEEP_WAKEUP_TOUCHPAD: //5
    Serial.println("Wakeup caused by touchpad");
    break;
  case ESP_SLEEP_WAKEUP_ULP: //6
    Serial.println("Wakeup caused by ULP program");
    break;
  default:
    Serial.printf("Wakeup was not caused by deep sleep: %d\n", wakeup_reason);
    break;
  }
}

void print_wakeup_touchpad()
{
  //touch_pad_t pin;

  wakeupTouchPin = esp_sleep_get_touchpad_wakeup_status();

  switch (wakeupTouchPin)
  {
  case 0:
    Serial.println("Touch 0 detected on GPIO 4");
    wakeupTouchGPIO = T0;
    break;
  case 1:
    Serial.println("Touch 1 detected on GPIO 0");
    wakeupTouchGPIO = T1;
    break;
  case 2:
    Serial.println("Touch 2 detected on GPIO 2");
    wakeupTouchGPIO = T2;
    break;
  case 3:
    Serial.println("Touch 3 detected on GPIO 15");
    wakeupTouchGPIO = T3;
    break;
  case 4:
    Serial.println("Touch 4 detected on GPIO 13");
    wakeupTouchGPIO = T4;
    break;
  case 5:
    Serial.println("Touch 5 detected on GPIO 12");
    wakeupTouchGPIO = T5;
    break;
  case 6:
    Serial.println("Touch 6 detected on GPIO 14");
    wakeupTouchGPIO = T6;
    break;
  case 7:
    Serial.println("Touch 7 detected on GPIO 27");
    wakeupTouchGPIO = T7;
    break;
  case 8:
    Serial.println("Touch 8 detected on GPIO 33");
    wakeupTouchGPIO = T8;
    break;
  case 9:
    Serial.println("Touch 9 detected on GPIO 32");
    wakeupTouchGPIO = T9;
    break;
  default:
    Serial.println("Wakeup not by touchpad");
    wakeupTouchGPIO = 0;
    break;
  }
}

void print_reset_reason(RESET_REASON reason)
{
  switch (reason)
  {
  case 1:
    Serial.println("POWERON_RESET");
    break; /**<1, Vbat power on reset*/
  case 3:
    Serial.println("SW_RESET");
    break; /**<3, Software reset digital core*/
  case 4:
    Serial.println("OWDT_RESET");
    break; /**<4, Legacy watch dog reset digital core*/
  case 5:
    Serial.println("DEEPSLEEP_RESET");
    break; /**<5, Deep Sleep reset digital core*/
  case 6:
    Serial.println("SDIO_RESET");
    break; /**<6, Reset by SLC module, reset digital core*/
  case 7:
    Serial.println("TG0WDT_SYS_RESET");
    break; /**<7, Timer Group0 Watch dog reset digital core*/
  case 8:
    Serial.println("TG1WDT_SYS_RESET");
    break; /**<8, Timer Group1 Watch dog reset digital core*/
  case 9:
    Serial.println("RTCWDT_SYS_RESET");
    break; /**<9, RTC Watch dog Reset digital core*/
  case 10:
    Serial.println("INTRUSION_RESET");
    break; /**<10, Instrusion tested to reset CPU*/
  case 11:
    Serial.println("TGWDT_CPU_RESET");
    break; /**<11, Time Group reset CPU*/
  case 12:
    Serial.println("SW_CPU_RESET");
    break; /**<12, Software reset CPU*/
  case 13:
    Serial.println("RTCWDT_CPU_RESET");
    break; /**<13, RTC Watch dog Reset CPU*/
  case 14:
    Serial.println("EXT_CPU_RESET");
    break; /**<14, for APP CPU, reseted by PRO CPU*/
  case 15:
    Serial.println("RTCWDT_BROWN_OUT_RESET");
    break; /**<15, Reset when the vdd voltage is not stable*/
  case 16:
    Serial.println("RTCWDT_RTC_RESET");
    break; /**<16, RTC Watch dog reset digital core and rtc module*/
  default:
    Serial.println("NO_MEAN");
  }
}

void connectToMqtt()
{
  if (!mqttClient.connected())
  {
    Serial.println("Connecting to MQTT...");
    mqttClient.setMaxTopicLength(MAXTOPICLENGTH);
    mqttClient.connect();
  }
  else
  {
    Serial.println("MQTT already connected!");
  }
}

void WiFiEvent(WiFiEvent_t event)
{
  Serial.printf("[WiFi-event] event: %d\n", event);
  switch (event)
  {
  case SYSTEM_EVENT_STA_GOT_IP:
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
    rssi = WiFi.RSSI();
    Serial.print("RSSI:");
    Serial.println(rssi);
    connectToMqtt();
    break;
    /*case SYSTEM_EVENT_STA_DISCONNECTED:
        Serial.println("WiFi lost connection");
        xTimerStop(mqttReconnectTimer, 0); // ensure we don't reconnect to MQTT while reconnecting to Wi-Fi
		    xTimerStart(wifiReconnectTimer, 0);
        break;*/
  }
}

float getADC()
{
  // Berechnung fuer Spannungsteiler 1M Ohm - 1M Ohm
  float VBAT = (float)(analogRead(adcpin)) / 4095.0 * 2 * 3.3 * (1.1 + 0.0);
  Serial.print("Vbat = ");
  Serial.print(VBAT);
  Serial.println(" Volts");
  return VBAT;
}

uint16_t publishButton(uint8_t button, BUTTON_STATE value)
{
  String topicPrefix = String(myhostConfig.configTopic);
  String payload;
  if (value != BUTTON_STATE::OFF)
  {
    if (value == BUTTON_STATE::SHORT_PRESSED)
    {
      payload = String(myappConfig.buttons[button].Value);
    }
    else if (value == BUTTON_STATE::LONG_PRESSED)
    {
      payload = String(myappConfig.buttons[button].LongpressValue);
    }
    uint16_t packetIdPub = mqttClient.publish(
        myappConfig.buttons[button].Topic, 1, false,
        payload.c_str());
    Serial.print("Publishing at QoS 1, packetId: ");
    Serial.println(packetIdPub);
  }

  uint16_t packetIdPub = mqttClient.publish(
      String(topicPrefix + "/button_" + button).c_str(), 1, false,
      String((uint8_t) value).c_str());
  Serial.print("Publishing at QoS 1, packetId: ");
  Serial.println(packetIdPub);
  //last_mqtt_package_id = packetIdPub;
  return packetIdPub;
}

void getTempHall(){
  uint8_t temperature_f = temprature_sens_read();
  temperature = (temperature_f - 32) / 1.8;
  Serial.print(temperature_f);
  Serial.print(" F  ");
  //  Serial.print((temperature - 32) / 1.8);
  Serial.print(temperature);
  Serial.println(" C");
  hall = hallRead();
  Serial.printf("Hall sensor: %d\n", hall);
}

uint16_t publishStatus()
{
  String topicPrefix = String(myhostConfig.configTopic);

  float vbat = getADC();
  getTempHall();

  StaticJsonDocument<200> doc;
  doc["vbat_begin"] = startvbat;
  doc["vbat_late"] = vbat;
  doc["bootcount"] = bootCount;
  doc["esp_timer_time"] = esp_timer_time;
  doc["rssi"] = rssi;
  doc["temperature"] = temperature;
  doc["hall"] = hall;
  doc["version"] = version;
  doc["buildtime"] = buildtime;
  doc["reset_reason_0"] = (uint8_t) rtc_get_reset_reason(0);
  doc["reset_reason_1"] = (uint8_t) rtc_get_reset_reason(1);
  doc["wakeup_reason"] = (uint8_t) wakeup_reason;

  String payload;
  serializeJson(doc, payload);
  uint16_t packetIdPub1 = mqttClient.publish(
      String(topicPrefix + "/status").c_str(), 1, true,
      payload.c_str());
  Serial.print("Publishing at QoS 1, packetId: ");
  Serial.println(packetIdPub1);
  return packetIdPub1;
}

void onMqttConnect(bool sessionPresent)
{
  Serial.println("Connected to MQTT.");
  Serial.print("Session present: ");
  Serial.println(sessionPresent);

  String topicPrefix = String(myhostConfig.configTopic);

  uint16_t packetIdSub2 = mqttClient.subscribe(
      String(topicPrefix + "/config").c_str(), 2);
  Serial.print("Subscribing at QoS 2, packetId: ");
  Serial.println(packetIdSub2);

  uint16_t packetIdSub3 = mqttClient.subscribe(
      String(topicPrefix + "/trigger").c_str(), 2);
  Serial.print("Subscribing at QoS 2, packetId: ");
  Serial.println(packetIdSub3);

  last_mqtt_package_id = publishStatus();

  if (myappConfig.crc != 0)
  {
    for (uint8_t button = 0; button < BUTTONCOUNT; button++)
    {
      if (buttonPressed[button] && String(myappConfig.buttons[button].Topic) != "")
      {
        last_mqtt_package_id = publishButton(button, BUTTON_STATE::SHORT_PRESSED);
      }
    }
  }
}

void setSleepTime(uint64_t seconds)
{
  if (esp_sleep_enable_timer_wakeup(seconds * uS_TO_S_FACTOR) == ESP_OK)
  {
    Serial.println("Setup ESP32 to sleep for every " + String((uint32_t)seconds) + " Seconds");
  }
  else
  {
    Serial.println("Setup ESP32 to sleep failed, use default");
    esp_sleep_enable_timer_wakeup(DEFAULT_TIME_TO_SLEEP * uS_TO_S_FACTOR);
  }
}

void touchCallback()
{
  //placeholder callback function
}

void goSleep()
{
  if (doing_update)
  {
    Serial.println("no sleep during update");
    return;
  }
  Serial.print("Going to sleep now. Runtime: ");

  mqttClient.disconnect();

  for (uint8_t button = 0; button < BUTTONCOUNT; button++)
  {
    //Setup interrupt on Touch Pad
    touchAttachInterrupt(buttonsConfig[button].touch, touchCallback, myappConfig.touchWakeupThreshold);
  }
  for (uint8_t button = 0; button < BUTTONCOUNT; button++)
  {
    digitalWrite(buttonsConfig[button].ledPower, LOW);
    pinMode(buttonsConfig[button].ledPower, INPUT);
  }

  Serial.println((uint32_t)(esp_timer_get_time() / 1000));
  Serial.flush();
  esp_timer_time = (uint32_t)(esp_timer_get_time() / 1000);
  esp_deep_sleep_start();
  Serial.println("This will never be printed");
}

void onMqttSubscribe(uint16_t packetId, uint8_t qos)
{
  Serial.println("Subscribe acknowledged.");
  Serial.print("  packetId: ");
  Serial.println(packetId);
  Serial.print("  qos: ");
  Serial.println(qos);
}

void onMqttUnsubscribe(uint16_t packetId)
{
  Serial.println("Unsubscribe acknowledged.");
  Serial.print("  packetId: ");
  Serial.println(packetId);
}

void saveAppConfig()
{
  unsigned long newcrc = crc((uint8_t *)&myappConfig, sizeof(appConfig));
  if (newcrc != myappConfig.crc)
  {
    myappConfig.crc = newcrc;
    EEPROM.put(sizeof(hostConfig), myappConfig);
    EEPROM.commit();
    Serial.println(myappConfig.crc);
  }
}

void onMqttMessage(char *topic, char *payload, AsyncMqttClientMessageProperties properties, size_t len, size_t index, size_t total)
{
  Serial.print("Publish received, ");
  Serial.print("  topic: ");
  Serial.print(topic);
  Serial.print(",  qos: ");
  Serial.print(properties.qos);
  Serial.print(",  dup: ");
  Serial.print(properties.dup);
  Serial.print(",  retain: ");
  Serial.print(properties.retain);
  Serial.print(",  len: ");
  Serial.print(len);
  Serial.print(",  index: ");
  Serial.print(index);
  Serial.print(",  total: ");
  Serial.print(total);
  if (len < 20)
  {
    Serial.print("  payload: ");

    char s_payload[len + 1];
    s_payload[len] = '\0';
    strncpy(s_payload, payload, len);
    Serial.println(s_payload);
  }
  else
  {
    Serial.println("  payload len>60");
    /*
    char s_payload[len+1];
    s_payload[len] = '\0';
    strncpy(s_payload, payload, len);
    */
  }

  String topicPrefix = String(myhostConfig.configTopic);

  if (String(topic) == String(topicPrefix + "/config") && total > 0)
  {
    if (index == 0)
    {
      jsonMessage = malloc(total + 1);
      memset(jsonMessage + total, 0, 1);
      //jsonMessage[total + 1] = '\0';
    }
    memcpy(jsonMessage + index, payload, len);
    if (index + len == total)
    {
      //DynamicJsonBuffer jsonBuffer;
      DynamicJsonDocument root(total + 128);
      //JsonObject &root = jsonBuffer.parseObject((char *)jsonMessage);
      auto jsonerror = deserializeJson(root, jsonMessage);
      if (!jsonerror)
      {
        Serial.println("JSON parse success");
        JsonObject rootobj = root.as<JsonObject>();
        if (rootobj.containsKey("touch_wakeup_threshold"))
        {
          uint16_t threshold = rootobj["touch_wakeup_threshold"];
          Serial.print("set new touch threshold wakeup config: ");
          Serial.println(threshold);
          myappConfig.touchWakeupThreshold = threshold;
        }
        if (rootobj.containsKey("touch_threshold"))
        {
          uint16_t threshold = rootobj["touch_threshold"];
          Serial.print("set new touch threshold config: ");
          Serial.println(threshold);
          myappConfig.touchThreshold = threshold;
        }
        if (rootobj.containsKey("led_brightness"))
        {
          uint8_t led_brightness = rootobj["led_brightness"];
          Serial.print("set new led brightness config: ");
          Serial.println(led_brightness);
          myappConfig.led_brightness = led_brightness;
        }
        if (rootobj.containsKey("time_to_sleep"))
        {
          uint32_t sleepseconds = rootobj["time_to_sleep"];
          if (sleepseconds > 3)
          {
            Serial.print("set new sleeptime from config: ");
            Serial.println(sleepseconds);
            myappConfig.time_to_sleep = sleepseconds;
            setSleepTime(sleepseconds);
          }
        }
        if (rootobj.containsKey("firmware_topic"))
        {
          String firmwaretopic = rootobj["firmware_topic"];
          if (firmwaretopic.length() < MAXTOPICLENGTH)
          {
            Serial.print("set new firmware topic from config: ");
            Serial.println(firmwaretopic);
            strcpy(myappConfig.firmwaretopic, firmwaretopic.c_str());
          }
        }

        if (rootobj.containsKey("button"))
        {
          Serial.println("Contains Key button");
          JsonObject buttons = rootobj["button"];
          for (uint8_t button = 0; button < BUTTONCOUNT; button++)
          {
            if (buttons.containsKey(String(button)))
            {
              Serial.print("Contains Button: ");
              Serial.println(button);
              JsonObject buttonConf = buttons[String(button)];
              if (buttonConf.containsKey("topic"))
              {
                strcpy(myappConfig.buttons[button].Topic, buttonConf["topic"]);
                Serial.print("topic:");
                Serial.println(myappConfig.buttons[button].Topic);
                if (buttonConf.containsKey("value"))
                {
                  strcpy(myappConfig.buttons[button].Value, buttonConf["value"]);
                  Serial.print("value:");
                  Serial.println(myappConfig.buttons[button].Value);
                }
                if (buttonConf.containsKey("longpressvalue"))
                {
                  strcpy(myappConfig.buttons[button].LongpressValue, buttonConf["longpressvalue"]);
                  Serial.print("long:");
                  Serial.println(myappConfig.buttons[button].LongpressValue);
                }
              }
            }
          }
        }
        saveAppConfig();
      }

      else
      {
        Serial.print("JSON parse failed: ");
        Serial.println(jsonerror.c_str());
      }
    }
    free(jsonMessage);
  }

  if (String(topic) == String(topicPrefix + "/trigger") && total > 0)
  {
    doing_update = true;
    char s_payload[len + 1];
    s_payload[len] = '\0';
    strncpy(s_payload, payload, len);

    uint16_t packetIdPub1 = mqttClient.publish(
        String(topicPrefix + "/trigger").c_str(), 2, true);
    Serial.print("Publishing at QoS 1, packetId: ");
    Serial.println(packetIdPub1);

    if (String(s_payload) == String("forceconfig"))
    {
      force_config = true;
    }
    if (String(s_payload) == String("firmwareupdate") &&
        String(myappConfig.firmwaretopic) != "")
    {
      doing_update = true;
      do_subscripe_firmwaretopic_id = packetIdPub1;
    }
  }

  if (String(myappConfig.firmwaretopic) != "" &&
      String(topic) == String(myappConfig.firmwaretopic) &&
      total > 0)
  {
    doing_update = true;
    int _lastError = 0;
    // start update
    if (index == 0)
    {
      //Update.runAsync(true);
      if (!Update.begin(total, U_FLASH))
      {
        _lastError = Update.getError();
#ifdef ENABLE_PRINT
        Update.printError(Serial);
#endif
        // ignore the rest
        topic[0] = 0;
      }
    }
    // do update in chunks
    if (!_lastError)
    {
      if (Update.write((uint8_t *)payload, len) != len)
      {
        _lastError = Update.getError();
#ifdef ENABLE_PRINT
        Update.printError(Serial);
#endif
        // ignore the rest
        topic[0] = 0;
      }
    }
    // finish update
    if (!_lastError)
    {
      if (index + len == total)
      {
        if (!Update.end())
        {
          _lastError = Update.getError();
#ifdef ENABLE_PRINT
          Update.printError(Serial);
#endif
        }
        else
        {
          // restart?
          //ESP.restart();
          doing_update = false;
          setSleepTime(5);
          goSleep();
        }
      }
    }
    else
    {
      doing_update = false;
    }
  }
}

void onMqttPublish(uint16_t packetId)
{
  Serial.println("Publish acknowledged.");
  Serial.print("  packetId: ");
  Serial.println(packetId);
  last_publish_success_id = packetId;
  
  if (do_subscripe_firmwaretopic_id == packetId &&
      String(myappConfig.firmwaretopic) != "")
  {
    uint16_t packetIdSub1 = mqttClient.subscribe(
        String(myappConfig.firmwaretopic).c_str(), 2);
    Serial.print("Subscribing at QoS 2, packetId: ");
    Serial.println(packetIdSub1);
    do_subscripe_firmwaretopic_id = 0;
    last_mqtt_package_id = 0;
  }
  if (last_mqtt_package_id == packetId &&
      wakeup_reason != ESP_SLEEP_WAKEUP_TOUCHPAD)
  {
    //mqttClient.disconnect();
    //goSleep();
    timeout_seconds = 1;
  }
}

bool setupWifiAndConfig(bool force)
{
  char get_ip[20] = "";
  char get_gw[20] = "";
  char get_sn[20] = "";
  char get_mqttHost[20] = "";
  char get_mqttPort[10] = "";
  char get_configTopic[50] = "";

  AsyncWiFiManagerParameter set_ip("IP", "ESP IP", get_ip, 20);
  AsyncWiFiManagerParameter set_gw("GW", "Gateway", get_gw, 20);
  AsyncWiFiManagerParameter set_sn("NM", "Netmasq", get_sn, 20);
  AsyncWiFiManagerParameter set_mqttHost("MQTT IP", "MQTT IP", get_mqttHost, 20);
  AsyncWiFiManagerParameter set_mqttPort("MQTT Port", "MQTT PORT 1883", get_mqttPort, 10);
  AsyncWiFiManagerParameter set_configTopic("Topic", "Topic with Configuration", get_configTopic, 50);

  AsyncWiFiManager wifiManager(&server, &dns);

  wifiManager.setConfigPortalTimeout(300);
  //set config save notify callback
  wifiManager.setSaveConfigCallback(saveConfigCallback);

  wifiManager.addParameter(&set_ip);
  wifiManager.addParameter(&set_gw);
  wifiManager.addParameter(&set_sn);
  wifiManager.addParameter(&set_mqttHost);
  wifiManager.addParameter(&set_mqttPort);
  wifiManager.addParameter(&set_configTopic);

  bool success = false;
  if (force)
  {
    wifiManager.resetSettings();
    success = wifiManager.startConfigPortal("OnDemandAP");
  }
  else
  {
    wifiManager.setSTAStaticIPConfig(myhostConfig._ip, myhostConfig._gw, myhostConfig._sn);
    success = wifiManager.autoConnect("ESPButtonConfig");
  }
  if (success && shouldSaveConfig)
  {
    myhostConfig._ip.fromString(String(set_ip.getValue()));
    myhostConfig._gw.fromString(String(set_gw.getValue()));
    myhostConfig._sn.fromString(String(set_sn.getValue()));
    myhostConfig.mqttHost.fromString(String(set_mqttHost.getValue()));
    myhostConfig.mqttPort = atoi(set_mqttPort.getValue());
    strcpy(myhostConfig.configTopic, set_configTopic.getValue());

    myhostConfig.crc = crc((uint8_t *)&myhostConfig, sizeof(hostConfig));
    EEPROM.put(0, myhostConfig);
    EEPROM.commit();
    Serial.println(myhostConfig.crc);
  }

  return success;
}

void setup()
{
  WRITE_PERI_REG(RTC_CNTL_BROWN_OUT_REG, 0); //disable brownout detector

  pinMode(adcpin, INPUT);
  adcAttachPin(adcpin);
  //analogReadResolution(12);
  analogSetAttenuation(ADC_11db);
  analogRead(adcpin);

  Serial.begin(115200);
  Serial.setDebugOutput(true);

  startvbat = getADC();//(float)(analogRead(adcpin)) / 4095.0 * 2 * 3.3 * (1.1 + 0.0);
  Serial.print("start Vbat = ");
  Serial.print(startvbat);
  Serial.println(" Volts");

  getTempHall();

  for (uint8_t button = 0; button < BUTTONCOUNT; button++)
  {
    pinMode(buttonsConfig[button].logic, INPUT_PULLUP);
    pinMode(buttonsConfig[button].ledPower, OUTPUT);
    digitalWrite(buttonsConfig[button].ledPower, HIGH);

  }
  //FastLED.addLeds<WS2812, DATA_PIN, RGB>(leds, NUM_LEDS);
  FastLED.addLeds<WS2812, LED_PIN_DATA, COLORSORT>(leds, BUTTONCOUNT);

  timeout_millis = millis();

  EEPROM.begin(sizeof(hostConfig) + sizeof(appConfig));

  Serial.print("Version: ");
  Serial.print(version);
  Serial.print(" ");
  Serial.println(buildtime);

  Serial.println("CPU0 reset reason: ");
  print_reset_reason(rtc_get_reset_reason(0));

  Serial.println("CPU1 reset reason: ");
  print_reset_reason(rtc_get_reset_reason(1));

  ++bootCount;
  Serial.println("Boot number: " + String(bootCount));

  esp_sleep_enable_touchpad_wakeup();


  EEPROM.get(0, myhostConfig);
  EEPROM.get(sizeof(hostConfig), myappConfig);

  if (myappConfig.crc == crc((uint8_t *)&myappConfig, sizeof(appConfig)))
  {
    Serial.println(F("Valid APP Config readed"));
    Serial.println(myappConfig.crc);
    Serial.println(myappConfig.time_to_sleep);
    setSleepTime(myappConfig.time_to_sleep);
    for (uint8_t button = 0; button < BUTTONCOUNT; button++)
    {
      Serial.println(myappConfig.buttons[button].Topic);
      Serial.println(myappConfig.buttons[button].Value);
      Serial.println(myappConfig.buttons[button].LongpressValue);
    }
  }
  else
  {
    Serial.println(F("No Valid APP Config"));
    Serial.println(F("Set default sleep time"));
    setSleepTime(DEFAULT_TIME_TO_SLEEP);
  }

  //Print the wakeup reason for ESP32
  print_wakeup_reason();

  print_wakeup_touchpad();
  for (uint8_t button = 0; button < BUTTONCOUNT; button++)
  {
    if(buttonsConfig[button].touch == wakeupTouchGPIO)
    {
      FastLED.setBrightness(myappConfig.led_brightness);
      leds[button] = CRGB::White;
      FastLED.show();

    }
  }

  for (uint8_t button = 0; button < BUTTONCOUNT; button++)
  {
    if (digitalRead(buttonsConfig[button].logic) == 0)
    {
      buttonPressed[button] = true;
      FastLED.setBrightness(myappConfig.led_brightness);
      leds[button] = CRGB::Red;
      FastLED.show();
    }
  }

  for (uint8_t button = 0; button < BUTTONCOUNT; button++)
  {
    Serial.print("Touch ");
    Serial.print(button);
    Serial.print(" :");
    Serial.println(touchRead(buttonsConfig[button].touch));
  }

  if (myhostConfig.crc == crc((uint8_t *)&myhostConfig, sizeof(hostConfig)))
  {
    Serial.println(F("Valid Host Config readed"));
    Serial.println(myhostConfig.crc);

    /*
    mqttClient.setCleanSession(false); //wenn cleansession auf true kommt der folgende Fehler viel selterner
    
    Connecting to MQTT...
    Connected to MQTT.
    Session present: 1
    assertion "last_unsent->oversize_left >= oversize_used" failed: file "/Users/ficeto/Desktop/ESP32/ESP32/esp-idf-public/components/lwip/lwip/src/core/tcp_out.c", line 686, function: tcp_write
    abort() was called at PC 0x400e72c3 on core 1
    Backtrace: 0x4008d054:0x3ffc93a0 0x4008d285:0x3ffc93c0 0x400e72c3:0x3ffc93e0 0x40108271:0x3ffc9410 0x40133065:0x3ffc9480 0x400d5111:0x3ffc94c0 0x400d289a:0x3ffc94f0 0x401533a5:0x3ffc9680 0x400d66e7:0x3ffc96a0 0x40153519:0x3ffc96e0 0x400d6a96:0x3ffc9700 0x400d571a:0x3ffc9730 0x400d57aa:0x3ffc97a0 0x40133525:0x3ffc97c0 0x4013356d:0x3ffc9800 0x4013396e:0x3ffc9820 0x4008fbad:0x3ffc9850
    Rebooting...
    */
    mqttClient.onConnect(onMqttConnect);
    //mqttClient.onDisconnect(onMqttDisconnect);
    mqttClient.onSubscribe(onMqttSubscribe);
    mqttClient.onUnsubscribe(onMqttUnsubscribe);
    mqttClient.onMessage(onMqttMessage);
    mqttClient.onPublish(onMqttPublish);
    WiFi.onEvent(WiFiEvent);

    mqttClient.setServer(myhostConfig.mqttHost, myhostConfig.mqttPort);
    //wifiManager.autoConnect(myhostConfig.hostName);
    if (!setupWifiAndConfig(false))
    {
      Serial.println("failed to connect and hit timeout");
      goSleep();
    }
  }
  else
  {
    Serial.println(F("Invalid Config readed"));
    Serial.println(myhostConfig.crc);

    if (!setupWifiAndConfig(true))
    {
      Serial.println("failed to connect and hit timeout");
      delay(3000);
      //reset and try again, or maybe put it to deep sleep
      setSleepTime(10);
      goSleep();
    }
  }

  if (shouldSaveConfig)
  {
    setSleepTime(10);
    goSleep();
  }

  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.print("ESP Mac Address: ");
  Serial.println(WiFi.macAddress());
  Serial.print("Subnet Mask: ");
  Serial.println(WiFi.subnetMask());
  Serial.print("Gateway IP: ");
  Serial.println(WiFi.gatewayIP());
  //Serial.print("DNS: ");
  //Serial.println(WiFi.dnsIP());

/*
  float temp2 = temperatureReadFixed();
  Serial.print(temp2);
  Serial.println(" C (temp2)");
*/
  getTempHall();

  timeout_millis = millis();
}

void loop()
{
  for (uint8_t button = 0; button < BUTTONCOUNT; button++)
  {
    if (!buttonPressed[button] && 
    digitalRead(buttonsConfig[button].logic) == 0 &&
    (buttonPause[button] + 500) < millis())
    {
      buttonPressed[button] = true;
      last_mqtt_package_id = publishButton(button, BUTTON_STATE::SHORT_PRESSED);
      FastLED.setBrightness(myappConfig.led_brightness);
      leds[button] = CRGB::Red;
      FastLED.show();

    }
    if (buttonPressed[button] && digitalRead(buttonsConfig[button].logic) == 1)
    {
      buttonPause[button] = millis();
      if (last_publish_success_id == last_mqtt_package_id)
      {
              leds[button] = CRGB::Green;
      }
      else
      {
              leds[button] = CRGB::Black;
      }
      FastLED.setBrightness(myappConfig.led_brightness);
      FastLED.show();
    }
    if (digitalRead(buttonsConfig[button].logic) == 1)
    {
      buttonPressed[button] = false;
      buttonLongpressed[button] = millis();
    }
    else
    {
      if ((buttonLongpressed[button] + LONGPRESSMILLIS) < millis())
      {
        buttonLongpressed[button] = millis();
        last_mqtt_package_id = publishButton(button, BUTTON_STATE::LONG_PRESSED);
        FastLED.setBrightness(myappConfig.led_brightness);
        leds[button] = CRGB::Blue;
        FastLED.show();
      }
    }
  }

  if (millis() % 1000 == 0)
  {
    getTempHall();

    for (uint8_t button = 0; button < BUTTONCOUNT; button++)
    {
      if (touchRead(buttonsConfig[button].touch) < myappConfig.touchThreshold)
      {
        Serial.print("reset timeout from touch ");
        Serial.println(button);
        timeout_millis = millis();
      }
    }
  }

  if (millis() % 10000 == 0)
  {
    Serial.print("millis: ");
    Serial.println(millis());
    Serial.print("timeout_millies: ");
    Serial.println(timeout_millis);
    for (uint8_t button = 0; button < BUTTONCOUNT; button++)
    {
      Serial.print("Button: ");
      Serial.println(button);
      Serial.print("Touch (loop) ");
      Serial.println(touchRead(buttonsConfig[button].touch));
      Serial.print("Pin (loop) ");
      Serial.println(digitalRead(buttonsConfig[button].logic));
    }
  }
  //delay(30000);
  if (force_config)
  {
    setupWifiAndConfig(true);
    doing_update = false;
  }

  if (millis() > (300 * 1000))
  {
    doing_update = false;
  }
  if (millis() > (timeout_seconds * 1000 + timeout_millis) &&
      (millis() % 100) == 0)
  {
    Serial.println("Timeout, go to sleep");
    goSleep();
  }
}
